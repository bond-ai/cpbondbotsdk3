

Pod::Spec.new do |spec|

  
  spec.name         = "BondBot"
  spec.version      = "3.2.0"
  spec.summary      = "BondBot is iOS SDK from Bond.AI"
  spec.description  = "BondBot is iOS SDK from Bond.AI, can be integrated in iOS application."
  spec.homepage     = "http://bond.ai"
  spec.license      = "MIT"
  spec.license      = { :type => "MIT", :file => "LICENSE.txt" }
  spec.author             = { "Mahesh Balshetwar" => "mahesh@bond.ai" }
  spec.platform     = :ios, "11.0"
  spec.swift_versions = "5.4"
  spec.source       = { :git => "https://BondPranjal@bitbucket.org/bond-ai/cpbondbotsdk3.git", :tag => "3.2.0" }
  #spec.source_files  = 'BondBot.framework/**/*'
  spec.resources = "BondBot.xcframework/**/*.{png,ttf,json}"
  
  #spec.public_header_files = 'BondBot.framework/Headers/*.h'
  spec.ios.vendored_frameworks = 'BondBot.xcframework'
  #spec.ios.deployment_target = '11.0'
  #spec.exclude_files = "BondBot.framework/**/*.{plist}"
  spec.requires_arc = true
  
  spec.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
  spec.dependency "SwiftSignalRClient", "~> 0.7.0"
  
end
